using namespace std;
#include <iostream>
#include <list>

class Bug {
protected:
    int id;
    pair<int, int> position;
    int direction;
    int size;
    bool alive;
    list<pair<int, int>> path;
    virtual void move() {}
    bool isWayBlocked() {}

};